#define _DEFAULT_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>
#include <time.h>
#include <ctype.h>

#define PORT 8080
#define STRING_LEN 2048
#define PATH_LEN 4099

#define ROOT_UID 0
#define USER_UID 1

char status_message[STRING_LEN];
char username[STRING_LEN];
char path[STRING_LEN];
char userPermission[STRING_LEN];
char tempCommand[STRING_LEN];

// status_handler
#define CREATE_SUCCESS 0
#define ACCESS_SUCCESS 1
#define DROP_SUCCESS 2
#define GRANT_SUCCESS 3
#define INSERT_SUCCESS 4
#define ONLY_ROOT_MODE -1
#define CANNOT_OPEN -2
#define DATABASE_EXIST -3
#define TABLE_EXIST -4
#define DATABASE_NOT_EXIST -5
#define TABLE_NOT_EXIST -6
#define PERMISSION_DENIED -7
#define USER_NOT_EXIST -8
#define NO_DATABASE_SELECTED -9
#define INPUT_NOT_MATCH_COLUMN -10
#define INPUT_DATATYPE_NOT_MATCH -11
#define WRONG_COMMAND_INPUT -12

void status_handler(int errCode)
{
    bzero(status_message, STRING_LEN);
    switch (errCode)
    {
    case CREATE_SUCCESS:
        strcpy(status_message, "CREATE SUCCESS\n");
        break;
    case ACCESS_SUCCESS:
        strcpy(status_message, "ACCESS SUCCESS\n");
        break;
    case DROP_SUCCESS:
        strcpy(status_message, "DROP SUCCESS\n");
        break;
    case GRANT_SUCCESS:
        strcpy(status_message, "GRANT SUCCESS\n");
        break;
    case INSERT_SUCCESS:
        strcpy(status_message, "INSERT SUCCESS \n");
        break;
    case ONLY_ROOT_MODE:
        strcpy(status_message, "ONLY FOR ROOT MODE\n");
        break;
    case CANNOT_OPEN:
        strcpy(status_message, "CANNOT BE OPENED\n");
        break;
    case DATABASE_EXIST:
        strcpy(status_message, "YOUR DATABASE ALREADY EXIST\n");
        break;
    case TABLE_EXIST:
        strcpy(status_message, "YOUR TABLE ALREADY EXIST\n");
        break;
    case DATABASE_NOT_EXIST:
        strcpy(status_message, "YOUR DATABASE OR TABLE NOT EXIST\n");
        break;
    case TABLE_NOT_EXIST:
        strcpy(status_message, "YOUR DATABASE OR TABLE NOT EXIST\n");
        break;
    case PERMISSION_DENIED:
        strcpy(status_message, "PERMISSION DENIED\n");
        break;
    case USER_NOT_EXIST:
        strcpy(status_message, "USER DOES NOT EXIST\n");
        break;
    case NO_DATABASE_SELECTED:
        strcpy(status_message, "NO DATABASE SELECTED\n");
        break;
    case INPUT_NOT_MATCH_COLUMN:
        strcpy(status_message, "INPUT AMOUNT DOES NOT MATCH WITH COLUMN AMOUNT\n");
        break;
    case INPUT_DATATYPE_NOT_MATCH:
        strcpy(status_message, "DATATYPE NOT MATCH\n");
        break;
    case WRONG_COMMAND_INPUT:
        strcpy(status_message, "YOUR COMMAND IS NOT CORRECT\n");
        break;
    default:
        break;
    }
}

void create(char client_message[STRING_LEN], int uid)
{
    char cmd[STRING_LEN];
    strcpy(cmd, client_message);
    char *token = strtok(cmd, " ");
    if (token && !strcmp(token, "CREATE"))
    {
        token = strtok(NULL, " ");
        if (token && !strcmp(token, "USER"))
        {
            if (uid == ROOT_UID)
            {
                token = strtok(NULL, " ");
                if (token)
                {
                    char user[STRING_LEN];
                    strcpy(user, token);
                    token = strtok(NULL, " ");
                    if (token && !strcmp(token, "IDENTIFIED"))
                    {
                        token = strtok(NULL, " ");
                        if (token && !strcmp(token, "BY"))
                        {
                            token = strtok(NULL, " ");
                            if (token)
                            {
                                char pass[STRING_LEN];
                                strcpy(pass, token);
                                FILE *user_list = fopen("users_list.txt", "a");
                                fprintf(user_list, "%s\t%s\n", user, pass);
                                fclose(user_list);
                                status_handler(CREATE_SUCCESS);
                            }
                            else
                                status_handler(WRONG_COMMAND_INPUT);
                        }
                        else
                            status_handler(WRONG_COMMAND_INPUT);
                    }
                    else
                        status_handler(WRONG_COMMAND_INPUT);
                }
                else
                    status_handler(WRONG_COMMAND_INPUT);
            }
            else
                status_handler(ONLY_ROOT_MODE);
        }
        else if (token && !strcmp(token, "DATABASE"))
        {
            token = strtok(NULL, " ");
            if (token)
            {
                char database[STRING_LEN];
                strcpy(database, token);
                DIR *dir = opendir(database);
                if (dir)
                {
                    status_handler(DATABASE_EXIST);
                    closedir(dir);
                }
                else if (ENOENT == errno)
                {
                    mkdir(database, 0777);
                    strcat(database, "/usersPermissionList.txt");
                    FILE *fp = fopen(database, "a");
                    if (uid == ROOT_UID)
                        fprintf(fp, "%s\n", "root");
                    else
                    {
                        char user[STRING_LEN];
                        strcpy(user, username);
                        fprintf(fp, "%s\n", strtok(user, "\t"));
                    }
                    fclose(fp);
                    status_handler(CREATE_SUCCESS);
                }
                else
                    status_handler(CANNOT_OPEN);
            }
            else
                status_handler(WRONG_COMMAND_INPUT);
        }
        else if (token && !strcmp(token, "TABLE"))
        {
            token = strtok(NULL, " ");
            if (token && strlen(path) > 0)
            {
                char temp_table[strlen(token)];
                strcpy(temp_table, token);

                char table_path[PATH_LEN * 2];
                sprintf(table_path, "%s/%s.txt", path, temp_table);
                printf("%s\n", table_path);
                FILE *tablePath = fopen(table_path, "r");
                if (tablePath)
                {
                    fclose(tablePath);
                    status_handler(TABLE_EXIST);
                }
                char column_names[STRING_LEN];
                char column_data_types[STRING_LEN];
                bzero(column_names, STRING_LEN);
                bzero(column_data_types, STRING_LEN);
                token = strtok(NULL, " (");
                if (!token)
                {
                    status_handler(WRONG_COMMAND_INPUT);
                    return;
                }
                while (token)
                {
                    char col[STRING_LEN];
                    strcpy(col, token);
                    strcat(col, "\t");
                    strcat(column_names, col);

                    token = strtok(NULL, " ");
                    if (token)
                    {
                        char type[STRING_LEN];
                        strcpy(type, token);
                        int i = strlen(type);
                        while (!(type[i] >= 'a' && type[i] <= 'z') && !(type[i] >= 'A' && type[i] <= 'Z'))
                            type[i--] = '\0';

                        if (!strcmp(type, "int") || !strcmp(type, "float") || !strcmp(type, "string"))
                        {
                            strcat(type, "\t");
                            strcat(column_data_types, type);
                        }
                        else
                            return status_handler(WRONG_COMMAND_INPUT);
                    }
                    else
                        status_handler(WRONG_COMMAND_INPUT);
                    token = strtok(NULL, " ");
                }

                tablePath = fopen(table_path, "a");
                fprintf(tablePath, "%s\n%s\n", column_names, column_data_types);
                fclose(tablePath);
                status_handler(CREATE_SUCCESS);
            }
            else
                status_handler(WRONG_COMMAND_INPUT);
        }
        else
            status_handler(WRONG_COMMAND_INPUT);
    }
    else
        status_handler(WRONG_COMMAND_INPUT);
}

void use(char client_message[STRING_LEN], int uid)
{
    char command[STRING_LEN];
    strcpy(command, client_message);
    char *token = strtok(command, " ");
    if (token && !strcmp(token, "USE"))
    {
        token = strtok(NULL, " ");
        if (token)
        {
            char database_name[STRING_LEN];
            strcpy(database_name, token);
            strcat(database_name, "/usersPermissionList.txt");
            if (uid == ROOT_UID)
            {
                strcpy(path, token);
                strcpy(userPermission, database_name);
                status_handler(ACCESS_SUCCESS);
                return;
            }
            FILE *usersPermissionList = fopen(database_name, "r");
            if (usersPermissionList)
            {
                char user_check[STRING_LEN];
                char temp_user[STRING_LEN];
                strcpy(temp_user, username);
                strcpy(status_message, "ACCESS DENIED\n");

                while (fgets(user_check, STRING_LEN, usersPermissionList) != NULL)
                {
                    char *user_token = strtok(temp_user, "\t");
                    while (user_token[strlen(user_token) - 1] == '\n' || user_token[strlen(user_token) - 1] == '\t')
                        user_token[strlen(user_token) - 1] = '\0';

                    char *user_comp = strtok(user_check, "\t");
                    while (user_comp[strlen(user_comp) - 1] == '\n' || user_comp[strlen(user_comp) - 1] == '\t')
                        user_comp[strlen(user_comp) - 1] = '\0';

                    if (strcmp(user_token, user_comp) == 0)
                    {
                        strcpy(path, token);
                        strcpy(userPermission, database_name);
                        status_handler(ACCESS_SUCCESS);
                        return;
                    }
                }
                fclose(usersPermissionList);
            }
            else
                status_handler(CANNOT_OPEN);
        }
        else
            status_handler(WRONG_COMMAND_INPUT);
    }
    else
        status_handler(WRONG_COMMAND_INPUT);
}


int checkUserPermission(char permissionListPath[STRING_LEN], int uid)
{
    if (uid == ROOT_UID)
        return 1;

    FILE *permissionList = fopen(permissionListPath, "r");
    if(permissionList==NULL)
        return 0;
    
    char user_check[STRING_LEN];
    char temp_user[STRING_LEN];
    strcpy(temp_user, username);
    while (fgets(user_check, STRING_LEN, permissionList) != NULL)
    {
        char *user_token = strtok(temp_user, "\t");
        while (user_token[strlen(user_token) - 1] == '\n' || user_token[strlen(user_token) - 1] == '\t')
            user_token[strlen(user_token) - 1] = '\0';

        char *permission_token = strtok(user_check, "\t");
        while (permission_token[strlen(permission_token) - 1] == '\n' || permission_token[strlen(permission_token) - 1] == '\t')
            permission_token[strlen(permission_token) - 1] = '\0';

        if (strcmp(permission_token, user_token) == 0)
        {
            fclose(permissionList);
            return 1;
        }   
    }
    fclose(permissionList);
    return 0;
}

int checkUserExist(char usersPath[15], char user[STRING_LEN])
{
    FILE *users_list = fopen(usersPath, "r");
    if(users_list==NULL)
        return 0;
    
    char users_check[STRING_LEN];

    while (fgets(users_check, STRING_LEN, users_list) != NULL)
    {
        char *user_token = strtok(users_check, "\t");

        while (user_token[strlen(user_token) - 1] == '\n' || user_token[strlen(user_token) - 1] == '\t')
            user_token[strlen(user_token) - 1] = '\0';

        if (strcmp(user_token, user)==0)
        {
            fclose(users_list);
            return 1;
        }
    }
    fclose(users_list);
    return 0;
}

void drop(char client_message[STRING_LEN], int uid)
{
    char cmd[STRING_LEN];
    strcpy(cmd, client_message);
    char *token = strtok(cmd, " ");
    if (token && !strcmp(token, "DROP"))
    {
        token = strtok(NULL, " ");
        if (token && !strcmp(token, "DATABASE"))
        {
            token = strtok(NULL, " ");
            if (token)
            {
                char database[STRING_LEN];
                char database_path[STRING_LEN];
                strcpy(database, token);
                strcpy(database_path, token);
                DIR *dir = opendir(database);
                strcat(database_path, "/usersPermissionList.txt");
                if (dir)
                {
                    int check_permission = checkUserPermission(database_path, uid);
                    if (check_permission)
                    {
                        struct dirent *fileExist = readdir(dir);
                        char dbremove[PATH_LEN * 2];
                        while (fileExist)
                        {
                            if (fileExist->d_type == DT_REG)
                            {
                                bzero(dbremove, PATH_LEN * 2);
                                sprintf(dbremove, "%s/%s", database, fileExist->d_name);
                                remove(dbremove);
                            }
                            fileExist = readdir(dir);
                        }
                        remove(database);
                        bzero(path, STRING_LEN);
                        bzero(userPermission, STRING_LEN);
                        status_handler(DROP_SUCCESS);
                    }
                    else
                        status_handler(PERMISSION_DENIED);
                    closedir(dir);
                }
                else
                    status_handler(DATABASE_NOT_EXIST);
            }
            else
                status_handler(WRONG_COMMAND_INPUT);
        }
        else if (token && !strcmp(token, "TABLE"))
        {
            if (strlen(path) <= 0)
                status_handler(NO_DATABASE_SELECTED);
            else
            {
                token = strtok(NULL, " ");
                if (token)
                {
                    char tb_path[PATH_LEN * 2];
                    char tb[STRING_LEN];
                    strcpy(tb, token);
                    sprintf(tb_path, "%s/%s.txt", path, tb);
                    FILE *tb_exist = fopen(tb_path, "r");
                    if (tb_exist)
                    {
                        fclose(tb_exist);
                        remove(tb_path);
                        status_handler(DROP_SUCCESS);
                    }
                    else
                        status_handler(TABLE_NOT_EXIST);
                }
                else
                    status_handler(WRONG_COMMAND_INPUT);
            }
        }
        else if (token && !strcmp(token, "COLUMN"))
        {
            //NOT YET IMPLEMENTED
        }
        else
            status_handler(WRONG_COMMAND_INPUT);
    }
    else
        status_handler(WRONG_COMMAND_INPUT);
}

void grant(char client_message[STRING_LEN], int uid)
{
    if (uid == ROOT_UID)
    {
        char message[STRING_LEN];
        strcpy(message, client_message);
        char *token = strtok(message, " ");
        if (token && !strcmp(token, "GRANT"))
        {
            token = strtok(NULL, " ");
            if (token && !strcmp(token, "PERMISSION"))
            {
                token = strtok(NULL, " ");
                if (token)
                {
                    char grant_db[STRING_LEN];
                    strcpy(grant_db, token);
                    token = strtok(NULL, " ");
                    if (token && !strcmp(token, "INTO"))
                    {
                        token = strtok(NULL, " ");
                        if (token)
                        {
                            char temp_user[STRING_LEN];
                            strcpy(temp_user, token);
                            int isUserExist = checkUserExist("users_list.txt", temp_user);
                            if (isUserExist)
                            {
                                DIR *dir = opendir(grant_db);
                                if (dir)
                                {
                                    closedir(dir);
                                    strcat(grant_db, "/usersPermissionList.txt");
                                    FILE *usersPermissionList = fopen(grant_db, "a");
                                    if(usersPermissionList){
                                        fprintf(usersPermissionList, "%s\n", temp_user);
                                        fclose(usersPermissionList);
                                        status_handler(GRANT_SUCCESS);
                                        return;
                                    }
                                }
                                else
                                    status_handler(DATABASE_NOT_EXIST);
                            }
                            else
                                status_handler(USER_NOT_EXIST);
                        }
                        else
                            status_handler(WRONG_COMMAND_INPUT);
                    }
                    else
                        status_handler(WRONG_COMMAND_INPUT);
                }
                else
                    status_handler(WRONG_COMMAND_INPUT);
            }
            else
                status_handler(WRONG_COMMAND_INPUT);
        }
        else
            status_handler(WRONG_COMMAND_INPUT);
    }
    else
        status_handler(ONLY_ROOT_MODE);
}

int intValidate(char *str)
{
    int i = 0;
    while (str[i] != '\0')
    {
        if (!isdigit(str[i]))
            return 0;
        i++;
    }
    return 1;
}

int stringValidate(char *str)
{
    size_t length = strlen(str);
    if (length>=2 && str[0] == '\'' && str[length - 1] == '\'')
        return 1;
    return 0;
}

void insert(char client_message[STRING_LEN], int uid)
{
    if (strlen(path) <= 0 && strlen(userPermission) <= 0)
        status_handler(NO_DATABASE_SELECTED);
    else
    {
        char cmd[STRING_LEN];
        strcpy(cmd, client_message);
        char *token = strtok(cmd, " ");
        if (token && !strcmp(token, "INSERT"))
        {
            token = strtok(NULL, " ");
            if (token && !strcmp(token, "INTO"))
            {
                token = strtok(NULL, " ");
                if (token)
                {
                    char table[STRING_LEN];
                    strcpy(table, token);
                    char table_path[PATH_LEN * 2];
                    sprintf(table_path, "%s/%s.txt", path, table);
                    FILE *tb_exist = fopen(table_path, "r");
                    if (!tb_exist)
                    {
                        status_handler(TABLE_NOT_EXIST);
                        return;
                    }
                    else
                    {
                        char total_datatypes[STRING_LEN];
                        char datatypes_check[STRING_LEN];

                        fgets(total_datatypes, STRING_LEN, tb_exist);
                        strcpy(datatypes_check, total_datatypes);
                        fclose(tb_exist);

                        int countValue = 0;
                        

                        token = strtok(NULL, " (");
                        while (token)
                        {
                            if (strcmp(token, "\n"))
                                countValue++;
                            token = strtok(NULL, " (");
                        }
                        int countDatatypes = 0;
                        token = strtok(total_datatypes, "\t");
                        while (token)
                        {
                            if (strcmp(token, "\n"))
                                countDatatypes++;
                            token = strtok(NULL, "\t");
                        }

                        if (countValue != countDatatypes)
                        {
                            status_handler(INPUT_NOT_MATCH_COLUMN);
                            return;
                        }

                        char datatypes[countDatatypes][STRING_LEN];
                        token = strtok(datatypes_check, "\t");
                        countDatatypes = 0;
                        while (token)
                        {
                            if (strcmp(token, "\n"))
                            {
                                bzero(datatypes[countDatatypes], strlen(datatypes[countDatatypes]));
                                strcpy(datatypes[countDatatypes], token);
                                while (datatypes[countDatatypes][strlen(datatypes[countDatatypes]) - 1] == '\n' ||
                                       datatypes[countDatatypes][strlen(datatypes[countDatatypes]) - 1] == '\t' ||
                                       datatypes[countDatatypes][strlen(datatypes[countDatatypes]) - 1] == ')')
                                    datatypes[countDatatypes][strlen(datatypes[countDatatypes]) - 1] = '\0';
                            
                                countDatatypes++;
                            }
                            token = strtok(NULL, "\t");
                        }

                        char value[countValue][STRING_LEN];
                        countValue = 0;
                        strcpy(cmd, client_message);
                        char *token = strtok(cmd, " ");
                        token = strtok(NULL, " ");
                        token = strtok(NULL, " ");
                        token = strtok(NULL, " (");
                        while (token)
                        {
                            if (strcmp(token, "\n"))
                            {
                                bzero(value[countValue], strlen(value[countValue]));
                                strcpy(value[countValue], token);
                                while (value[countValue][strlen(value[countValue]) - 1] == '\n' ||
                                       value[countValue][strlen(value[countValue]) - 1] == '\t' ||
                                       value[countValue][strlen(value[countValue]) - 1] == ' ' ||
                                       value[countValue][strlen(value[countValue]) - 1] == ',' ||
                                       value[countValue][strlen(value[countValue]) - 1] == ')' ||
                                       value[countValue][strlen(value[countValue]) - 1] == ';')
                                    value[countValue][strlen(value[countValue]) - 1] = '\0';
                                countValue++;
                            }
                            token = strtok(NULL, " ");
                        }

                        // data type check with value
                        char insert_value[STRING_LEN];
                        bzero(insert_value, STRING_LEN);
                        for (int i = 0; i < countDatatypes; i++)
                        {
                            if (!strcmp(datatypes[i], "int"))
                            {
                                if (!intValidate(value[i]))
                                {
                                    status_handler(INPUT_DATATYPE_NOT_MATCH);
                                    return;
                                }
                                else
                                {
                                    strcat(insert_value, value[i]);
                                    strcat(insert_value, "\t");
                                }
                            }
                            else if (!strcmp(datatypes[i], "string"))
                            {
                                if (!stringValidate(value[i]))
                                {
                                    status_handler(INPUT_DATATYPE_NOT_MATCH);
                                    return;
                                }
                                else
                                {
                                    strcat(insert_value, value[i]);
                                    strcat(insert_value, "\t");
                                }
                            }
                        }
                        FILE *tb_exist = fopen(table_path, "a+");
                        fprintf(tb_exist, "%s\n", insert_value);
                        fclose(tb_exist);

                        status_handler(INSERT_SUCCESS);
                    }
                }
                else
                    status_handler(WRONG_COMMAND_INPUT);
            }
            else
                status_handler(WRONG_COMMAND_INPUT);
        }
        else
            status_handler(WRONG_COMMAND_INPUT);
    }
}

void userLog(int uid)
{
    time_t now;
    struct tm *_time;

    time(&now);
    _time = localtime(&now);
    char timestamp[STRING_LEN];
    strftime(timestamp, sizeof(timestamp), "%F %T", _time);

    char temp_[STRING_LEN];
    FILE *log_file = fopen("usersLog.txt", "a");
    if(uid == ROOT_UID){
        strcpy(temp_, "ROOT");
    }
    else{
        strcpy(temp_, username);
    }
    char *token = strtok(temp_, "\t");
    fprintf(log_file, "%s:%s:%s\n", timestamp, token, tempCommand);
    fclose(log_file);
}

int main()
{
    //CREATE AND SET DAEMON
    pid_t pid;
    pid = fork();

    if (pid < 0)
        exit(EXIT_FAILURE);

    if (pid > 0)
        exit(EXIT_SUCCESS);

    umask(0);
    pid_t sid = setsid();

    if (sid < 0)
        exit(EXIT_FAILURE);

    if ((chdir("/")) < 0)
        exit(EXIT_FAILURE);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    //CREATE  AND SET SOCKET
    int svr_sock = socket(AF_INET, SOCK_STREAM, 0);
    if(svr_sock<0){
        perror("SOCKET CREATE FAILED");
        exit(EXIT_FAILURE);
    }
    struct sockaddr_in svr_address;
    svr_address.sin_family = AF_INET;
    svr_address.sin_port = htons(PORT);
    svr_address.sin_addr.s_addr = INADDR_ANY;

    // BIND AND LISTEN
    int bind_stat = bind(svr_sock, (struct sockaddr *)&svr_address, sizeof(svr_address));
    if(bind_stat<0){
        perror("SOCKET BIND FAILED");
        exit(EXIT_FAILURE);
    }

    int listen_stat = listen(svr_sock, 5);
    if(listen_stat<0){
        perror("SOCKET LISTEN FAILED");
        exit(EXIT_FAILURE);
    }

    int client_sock;
    int addr_size = sizeof(svr_address);
    char server_message[STRING_LEN];
    char client_message[STRING_LEN];
    int uid;
    if(chdir("/home/victorgg345/Documents/SistemOperasi/FinalPraktikum/database")==-1)
    {
        perror("chdir failed");
        return 1;
    }
    // DAEMON LOOP
    while (1)
    {
        // DAEMON PROGRAM
        client_sock = accept(svr_sock, (struct sockaddr *)&svr_address, (socklen_t *)&addr_size);
        if (client_sock >= 0)
        {
            // GET USER TYPE
            recv(client_sock, client_message, STRING_LEN, 0);
            if (!strcmp(client_message, "ROOT"))
            {
                uid = ROOT_UID;
                bzero(server_message, STRING_LEN);
                strcpy(server_message, "ACCESS AS ROOT\n");
            }
            else
            {
                
                uid = USER_UID;
                bzero(server_message, STRING_LEN);
                bzero(username, STRING_LEN);
                strcpy(username, client_message);
                char username_check[STRING_LEN];
                int found = 0;
                
                fclose(fopen("users_list.txt", "a"));
                FILE *users_list = fopen("users_list.txt", "r");            
                while (fgets(username_check, STRING_LEN, users_list) != NULL)
                {
                    if (!strcmp(username_check, username))
                    {
                        
                        bzero(server_message, STRING_LEN);
                        strcpy(server_message, "ACCESS AS USER\n");
                        found = 1;
                        break;
                    }
                }
                if (!found)
                {
                    bzero(server_message, STRING_LEN);
                    strcpy(server_message, "USER NOT FOUND");
                }
                fclose(users_list);
            }
            send(client_sock, server_message, strlen(server_message), 0);
            if (!strcmp(server_message, "USER NOT FOUND"))
                continue;

            while (1)
            {
                bzero(client_message, STRING_LEN);
                recv(client_sock, client_message, STRING_LEN, 0);
                if (!strcmp(client_message, "EXIT"))
                    break;

                char cmd[STRING_LEN];
                strcpy(cmd, client_message);
                bzero(tempCommand, STRING_LEN);
                strcpy(tempCommand, client_message);
                // function create
                if (!strcmp(strtok(cmd, " "), "CREATE"))
                    create(client_message, uid);

                else if (!strcmp(strtok(cmd, " "), "USE"))
                    use(client_message, uid);

                else if (!strcmp(strtok(cmd, " "), "DROP"))
                    drop(client_message, uid);

                else if (!strcmp(strtok(cmd, " "), "GRANT"))
                    grant(client_message, uid);

                else if (!strcmp(strtok(cmd, " "), "INSERT"))
                    insert(client_message, uid);

                send(client_sock, status_message, strlen(status_message), 0);
                bzero(status_message, STRING_LEN);
                if (strlen(tempCommand) > 0)
                    userLog(uid);
            }
        }
    }
}