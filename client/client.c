#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>
#define LOCAL_PORT 8080
#define BUFFER_LEN 256
#define ERROR_SOCKET_CREATE -1
#define ERROR_SOCKET_CONNECT -2
#define ERROR_SOCKET_BIND -3
#define ERROR_SOCKET_LISTEN -4
#define ROOT_UID 0
#define EXIT_COMMAND "EXIT"

void error(int errcode)
{
    switch (errcode)
    {
    case ERROR_SOCKET_CREATE:
        printf("FAILED TO CREATE SOCKET\n");
        break;
    case ERROR_SOCKET_CONNECT:
        printf("FAILED TO CONNECT SOCKET\n");
        break;
    default:
        break;
    }

    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
    // CREATE SOCKET
    int clientSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (clientSocket < 0)
        error(ERROR_SOCKET_CREATE);

    // SET SOCKET ADDRESS UP
    struct sockaddr_in serverAdd;
    serverAdd.sin_family = AF_INET;
    serverAdd.sin_port = htons(LOCAL_PORT);
    serverAdd.sin_addr.s_addr = INADDR_ANY;

    // Connect to the server
    int connectionStat = connect(clientSocket, (struct sockaddr *)&serverAdd, sizeof(serverAdd));
    if (connectionStat < 0)
        error(ERROR_SOCKET_CONNECT);

    char clientMes[BUFFER_LEN];
    char serverMes[BUFFER_LEN];

    // Check if logged in as root or user
    int uid = getuid();
    if (uid == ROOT_UID)
    {
        printf("LOGGED IN AS ROOT\n");
        strncpy(clientMes, "ROOT", BUFFER_LEN - 1);
        clientMes[BUFFER_LEN - 1] = '\0';
        send(clientSocket, clientMes, BUFFER_LEN, 0);
    }
    else
    {
        printf("LOGGED IN AS USER\n");
        char user[BUFFER_LEN];
        strcpy(user, argv[2]);
        strcat(user, "\t");
        strcat(user, argv[4]);
        strcat(user, "\n");
        send(clientSocket, user, BUFFER_LEN, 0);
    }

    memset(serverMes, 0, BUFFER_LEN);
    int bytesRecv = recv(clientSocket, serverMes, BUFFER_LEN - 1, 0);
    if (bytesRecv < 0)
    {
        fprintf(stderr, "ERROR: Failed to receive message from server. Error code: %d\n", errno);
        close(clientSocket);
        exit(EXIT_FAILURE);
    }

    serverMes[bytesRecv] = '\0';
    if (!strcmp(serverMes, "USER NOT FOUND"))
    {
        printf("%s\n", serverMes);
        close(clientSocket);
        exit(EXIT_SUCCESS);
    }

    printf("\n\n%s\n\n", serverMes);

    while (1)
    {
        memset(clientMes, 0, BUFFER_LEN);
        memset(serverMes, 0, BUFFER_LEN);

        // Get user input
        fgets(clientMes, BUFFER_LEN, stdin);
        clientMes[strlen(clientMes) - 1] = '\0';

        // Send user input to server
        send(clientSocket, clientMes, strlen(clientMes), 0);

        if (!strcmp(clientMes, EXIT_COMMAND))
            break;

        // Receive and display server's response
        int totalRecv = 0;
        while (totalRecv < BUFFER_LEN - 1)
        {
            bytesRecv = recv(clientSocket, serverMes + totalRecv, 1, 0);
            if (bytesRecv <= 0)
            {
                break;
            }
            totalRecv += bytesRecv;
            if (serverMes[totalRecv - 1] == '\n')
            {
                break;
            }
        }
        if (bytesRecv < 0)
        {
            fprintf(stderr, "ERROR: Failed to receive message from server. Error code: %d\n", errno);
            close(clientSocket);
            exit(EXIT_FAILURE);
        }
        serverMes[totalRecv] = '\0';

        printf("%s\n", serverMes);
    }

    close(clientSocket);
    return 0;
}